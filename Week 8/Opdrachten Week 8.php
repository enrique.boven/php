<?php
//opdracht1
function printCitaat($sentence) {
    print("-- " . $sentence . " --\n");
}

//opdracht 2
function printCitaat2($auteur, $quote) {
    print("-- " . $auteur . ": " . $quote . " --\n");
}

//opdracht 3
function telOp($getal1, $getal2) {
    $total = 0;
    for($i = $getal1; $i <= $getal2; $i++) {
        $total += $i;
    }
    return $total;
}

//opdracht 4
function gemiddelde($getal1, $getal2) {
    return (($getal1 + $getal2) / 2);
}

//opdracht 5
function isSom($getal1, $getal2, $som) {
    if(($getal1 + $getal2) == $som) {
        print("Dit is de som van de 2 getallen");
    } else {
        print("Dit is niet de som van de 2 getallen");
    }
}

//opdracht 6
function printMail($naam, $dag, $isVerstuurd) {
    print("Beste " . $naam . ",\nJe pakket " . ($isVerstuurd ? "is" : "wordt") . " " . ($dag == "" ? "vandaag" : $dag) . " verzonden");
}

//opdracht 7
function opdracht7() {
    $hoofdstukken = array(
        "Voorwoord" => 5,
        "Inleiding" => 7,
        "Expressionisme" => 9,
        "Surrealisme" => 29,
        "Kubisme" => 53,
        "Dada" => 66,
        "Futurisme" => 90,
        "Nieuwe zakelijkheid" => 99,
        "De Stijl" => 121,
        "Cobra" => 144,
        "Literatuur" => 158);

    $aantalPuntjes = 30;
    foreach ($hoofdstukken as $titel => $pagina) {
        printHoofdstuk($titel, $pagina);
    }

    function printHoofdstuk($hoofdstuk, $pagina) {
        print($hoofdstuk . str_repeat(".", berekenAantalPuntjes($hoofdstuk, $pagina)) . $pagina . "\n");
    }

    function berekenAantalPuntjes($titel, $pagina) {
        global $aantalPuntjes;
        return ($aantalPuntjes - strlen($titel) - strlen($pagina));
    }
}

//opdracht 8
function tekenRondje() {
    print("O");
}

function tekenLijn($aantal) {
    for($i = 0; $i < $aantal; $i++) {
        tekenRondje();
    }
    print("\n");
}

function tekenRechthoek($lengte, $breedte) {
    for($i = 0; $i < $lengte; $i++) {
        tekenLijn($breedte);
    }
}

function tekenVierkant($lengte) {
    tekenRechthoek($lengte, $lengte);
}

function tekenDriehoek($lengte) {
    for($i = 1; $i <= $lengte; $i++) {
        tekenLijn($i);
    }
}

function tekenPiramide($lengte) {
    for($i = 0; $i < $lengte; $i++) {
        print(str_repeat(" ", $lengte - $i -1));
        print(str_repeat("*", ($i*2) + 1) . "\n");
    }
}

//opdracht 9
//tegel lengte en breedte
$tegelSize = array(15, 15);
//muur breedte en hoogte in meters
$size = array(5, 2.6);
//aantal tegels max per doos
$doosMax = 20;

$aantalTegels = berekenAantalTegels($size[0], $size[1]);
$aantalDozen = berekenAantalDozen($size[0], $size[1]);

print("Voor deze muur zijn " . $aantalTegels . " tegels nodig.\n");
print("Voor deze muur moet je " . $aantalDozen . " dozen kopen.\n");

function berekenAantalTegels($breedte, $hoogte) {
    global $tegelSize;
    return ceil((($breedte * $hoogte) / (($tegelSize[0] * $tegelSize[1]) / 10000)));
}

function berekenAantalDozen($breedte, $hoogte) {
    global $doosMax;
    return ceil((berekenAantalTegels($breedte, $hoogte) / $doosMax));
}

//opdracht 10
$woorden = array("lepel", "vork", "negen", "tien");
testZoekSpiegelwoorden($woorden, true);

function testZoekSpiegelWoorden(array $woorden, $spiegel) {
    print("\nDe volgende woorden zijn ". ($spiegel ? "" : " geen ") . "spiegelwoorden:\n");
    foreach ($woorden as $woord) {
            if(isSpiegelWoord($woord)) {
                if($spiegel) {
                    print($woord . "\n");
                }
            } else if(!$spiegel) {
                print($woord . "\n");
            }
    }
}
function isSpiegelWoord($woord) {
    return strrev($woord) == $woord;
}
