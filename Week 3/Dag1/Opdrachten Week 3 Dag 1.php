<?php
function opdracht2() {
    print("Hello world!");
}

function opdracht3() {
    print("Hoe heet je? ");
    $naam = fgets(STDIN);
    print("Hallo ". $naam);
}

function opdracht4() {
    $naam = "Erwin";
    print($naam);
}

function opdracht5() {
    $stripFiguur1 = "Donald";
    print("Favoriete stripfiguren: " . $stripFiguur1);
}

function opdracht6() {
    $naam = "Alwin de Vries";
    $straat = "Bomenlaan";
    $huisnummer = 12;

    $adres = $straat . " " . $huisnummer;

    print($naam . ", " . $adres);
}

function opdracht7() {
    print("Geef 2 getallen ");
    print("\n");
    $getal1 = trim(fgets(STDIN));
    $getal2 = trim(fgets(STDIN));
    $som = $getal1 + $getal2;
    print("getal1 = " . $getal1 . ", getal2 = " . $getal2 . " som = " . $som);
}

function opdracht8() {
    $gerecht1 = "Pizza";
    $prijs1 = 8.5;
    $gerecht2 = "Macaroni";
    $prijs2 = 5.25;
    $gerecht3 = "Lasagna";
    $prijs3 = 5.95;

    print($gerecht1 . " = " . $prijs1 . " Groot = ". $prijs1 * 2 . "\n");
    print($gerecht2 . " = " . $prijs2 . " Groot = ". $prijs2 * 2 . "\n");
    print($gerecht3 . " = " . $prijs3 . " Groot = ". $prijs3 * 2);
}

function opdracht9() {
    $s = "aa";
    print($s . " " .strlen($s) . "\n");

    $s = $s.$s;
    print($s . " " .strlen($s) . "\n");

    $s = $s.$s;
    print($s . " " .strlen($s) . "\n");
}