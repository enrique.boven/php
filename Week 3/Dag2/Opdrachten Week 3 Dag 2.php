<?php

print("Kies een tafel.");
$tafel = trim(fgets(STDIN));
print("Kies hoe lang de tafel moet lopen. ");
$range = trim(fgets(STDIN));

if(!is_numeric($tafel) or !is_numeric($range)) {print ("Verkeerd(e) getal(len) ingevoerd"); return;}
for ($x = 1; $x <= $range; $x++) {
    print($tafel . "x" . $x . "=" . $tafel*$x . "\n");
}