<?php
opdracht7();

function opdracht1() {
    $namen = array('Tobias', 'Hasna', 'Aukje', 'Fred');
    asort($namen);
    print_r($namen);

    foreach ($namen as $getal => $naam) {
        print($getal . ' : ' . $naam . "\n");
    }
}

function opdracht2() {
    $playlist = array();
    print("Nummer 1 = ");
    $playlist[0] = trim(fgets(STDIN));
    print("Nummer 2 = ");
    $playlist[1] = trim(fgets(STDIN));
    print("Nummer 3 = ");
    $playlist[2] = trim(fgets(STDIN));
    print_r($playlist);
}

function opdracht3() {
    $rondeTijden = array('5', '5', '6', '6', '7');
    $aantalRonden = count($rondeTijden);
    $totaleTijd = array_sum($rondeTijden);
    $snelsteRonde = max($rondeTijden);
    $langzaamsteRonde = min($rondeTijden);
    foreach ($rondeTijden as $ronde => $tijd) {
        print("Ronde " . ($ronde + 1) . ': ' . $tijd . " minuten.\n");
    }
    print("Aantal ronden: " . $aantalRonden . "\n");
    print("Totale tijd: " . $totaleTijd . " minuten\n");
    print("Snelste ronde: " . $snelsteRonde . " minuten\n");
    print("Langzaamste ronde: " . $langzaamsteRonde . " minuten\n");
}

function opdracht4() {
    $klassenlijst = array("tobias", "hasna", "aukje", "fred", "sep", "koen", "wahed", "anna", "jackie", "rashida", "winston", "sammy", "manon", "ben", "karim", "bart", "lisa");
    array_push($klassenlijst, "Tom");
    $aantal = count($klassenlijst);
    print("Aanta leerlingen: " . $aantal . "\n");
    foreach ($klassenlijst as $nr => $leerling) {
        print (($nr + 1) . ". " . ucfirst($leerling). "\n");
    }
}

function opdracht5() {
    $getallen = array(1, 8, 12, 7, 14, -13, 8, 1, -1, 14, 7);
    $highest = $getallen[0];
    foreach ($getallen as $getal) {
        if($getal > $highest) {
            $highest = $getal;
        }
    }
    print("Hoogste is " . $highest);
    $count = array_count_values($getallen);
    print("\nAantal keer voorkomend: " . $count[$highest]);
}

function show(array $array) {
    foreach ($array as $a => $b) {
        print($a . " " . $b . "\n");
    }
}

function opdracht6() {
    $telefoonBoek = array(
        'Mickey Mouse' => '038-4699776',
        'Guus Geluk' => '0578-121212',
        'Donald Duck' => '010-2311512');
    print("Wie Krijgt er een nieuw nummer?\n");
    $karakter = trim(fgets(STDIN));
    if(array_key_exists($karakter, $telefoonBoek)) {
        print("Typ nieuw telefoonnummer.\n");
        $telefoonnummer = trim(fgets(STDIN));
        $telefoonBoek[$karakter] = $telefoonnummer;
        print("Succesvol aangepast.\n");
        show($telefoonBoek);
    }else{
        print("Dat karakter bestaat niet");
    }
}

function opdracht7() {
    $prijzen = array('cola' => 2.50, 'koffie' => 2, 'thee' => 1.75, 'bier' => 2.25, 'wijn' => 3.75, 'water' => 0.5);
    $max = max($prijzen);
    $gemiddeld = (array_sum($prijzen) / sizeof($prijzen));
    foreach ($prijzen as $product => $prijs) {
        if($prijs == $max) {
            print("De hoogste prijs is " . $prijs . ", dit is van het product " . $product . "\n");
        }
    }
    print("Gemiddelde van alle producten is: " . $gemiddeld . "\n");
    $bestelling1 = array('cola', 'cola', 'bier', 'wijn', 'water', 'koffie', 'koffie', 'koffie');
    $prijs = 0;
    $bestellingMessage = "Bestelling: ";
    foreach ($bestelling1 as $product) {
        $prijs += $prijzen[$product];
        $bestellingMessage = $bestellingMessage . $product . " ";
    }
    print($bestellingMessage . "\n");
    print("Totaal prijs: " . $prijs . "\n");
}
