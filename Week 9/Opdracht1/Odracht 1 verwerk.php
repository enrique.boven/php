<?php
if(!isset($_GET["naam"])) {
    print("Maam invullen is verplicht!");
}
elseif(!isset($_GET["leeftijd"])) {
    print("Leeftijd invullen is verplicht!");
}
else{
    $naam = $_GET["naam"];
    $leeftijd = $_GET["leeftijd"];
    if($leeftijd < 0) {
        print("Leeftijd moet groter dan of gelijk aan 0 zijn."); return;
    }
    if($leeftijd < 18) {
        print("Over " . (18 - $leeftijd) . " jaar wordt " . $naam . " volwassen."); return;
    }
    if(isBetween($leeftijd, 21, 65)) {
        print($naam . " is nu pas echt volwassen."); return;
    }
}

function isBetween($num, $range1, $range2) {
    return $num >= $range1 && $num <= $range2;
}