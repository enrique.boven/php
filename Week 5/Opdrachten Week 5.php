<?php

opdracht2();

function opdracht1() {
    $squareSize = 5;
    for($i = 0; $i < $squareSize; $i++) {
        for($i2 = 0; $i2 < $squareSize; $i2++) {
            print(rand(1, 100) . " ");
        }
        print("\n");
    }
}

function opdracht2() {
    $aantalKaarten = 0;
    $aantalPunten = 0;
    while((rand(1,6)) % 2 == 0) {
        $aantalKaarten += 1;
        for($i = 0; $i < $aantalKaarten; $i++) {
            $randInt = rand(1,6);
            print("\nHet getrokken getal is " . $randInt);
            for($k = 0; $k < $randInt; $k++) {
                $dobbel = rand(1,52);
                print("\nDe waarde van de kaart is: " . $dobbel);
                $aantalPunten += $dobbel;
            }
        }
    }
    print("\nDe totale waarde is: " . $aantalPunten);
}

function opdracht3() {
    $startVermogen = 1000;
    $rentePercentage = 4;
    $jaren = 0;
    print("\nNa hoeveel jaar is mijn vermogen verdubbeld bij een bepaalde rente?\n");
    print("\nHet startvermogen is: " . $startVermogen . "\n");
    print("De rente is: " . $rentePercentage . "%\n");
    while($startVermogen < 2000) {
        $startVermogen *= 1 + ($rentePercentage/100);
        $jaren += 1;
    }
    print("\nHet eindvermogen is: " . $startVermogen . "\n");
    print("Het aantal jaren is: " . $jaren . "\n");

}
