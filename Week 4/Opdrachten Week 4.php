<?php
opdracht14();

function opdracht1() {
    print("Geef vogels op. ");
    $aantal = trim(fgets(STDIN));
    print ("Ik zie " . $aantal . " vogel" . ($aantal > 1 ? "s" : ""));
}

function opdracht2() {
    print("type ik, jij of hij\n");
    $persoon = trim(fgets(STDIN));
    $verjaardag = "16 april";
    $zin = " jarig op " . $verjaardag;
    switch ($persoon) {
        case 'ik': $zin = 'ik ben' . $zin; break;
        case 'jij': $zin = ' jij bent' . $zin; break;
        case 'hij': $zin = ' hij is' . $zin; break;
        default: $zin = 'Verkeerd';
    }
    print($zin);
}

function opdracht3() {
    print("Hoe oud ben je?\n");
    $leeftijd = trim(fgets(STDIN));
    if(!is_numeric($leeftijd)) { opdracht3(); }
    print ("Je bent " . $leeftijd . ", dus je mag " . ($leeftijd >= 18 ? 'wel' : 'niet') ." stemmen");
}

function opdracht4() {
    print("Hoe warm wordt het?\n");
    $temperatuur = trim(fgets(STDIN));
    if(!is_numeric($temperatuur)) {opdracht4();}
    print($temperatuur > 25 ? "We gaan naar het strand." : "We blijven thuis.");
}

function opdracht5() {
    $anna = 78;
    $bob = 78;
    $zin = "Anna is ";
    if($anna > $bob) { $zin = $zin . "ouder dan Bob.";}
    if($anna < $bob) { $zin = $zin . "jonger dan Bob";}
    if($anna == $bob) {$zin = $zin . "even oud als Bob";}
    print($zin);
}


function opdracht6() {
    $boterhammenSjors = 2;
    $boterhammenMathilde = 3;
    $boterhammenSharon = 4;
    $gemiddelde = ($boterhammenMathilde + $boterhammenSharon + $boterhammenSjors) / 3;
    print("Gemiddeld worden er " . $gemiddelde . " boterhammen gegeten.\n");
    print("Sjors eet " . $boterhammenSjors . " boterhammen\n");
    print("Mathilde eet " . $boterhammenMathilde . " boterhammen\n");
    print("Sharon eet " . $boterhammenSharon . " boterhammen");
}

function opdracht7() {
    $stelling1 = "Olifant schrijf je met een O";
    $waarheid1 = True;
    $stelling2 = "Programmeren is saai.";
    $waarheid2 = False;
    if ($waarheid1) {
        print ("Olifant schrijf je met een O ja dat klopt.");
    } else {
        print ("Niet waar");
    }
    if($waarheid2) {
        print ("Programmeren is inderdaar saai.");
    } else{
        print ("Programmeren is niet saai!");
    }
}

function opdracht8() {
    $regen = True;
    $meerijden = False;
    if($regen and $meerijden) {
        print ("Vandaag rijd ik met iemand mee.");
    }
    else if($regen and !$meerijden) {
        print("Ik ga met de bus");
    }
    else{
        print("Ik ga fietsen");
    }
}

function opdracht9() {
    $prijsPerKM = 0.1;
    $studentWeekAbbo = True;
    $studentWeekendAbbo = True;
    $weekend = True;
    $feestdag = True;

    if($weekend) {
        if($studentWeekendAbbo || ($studentWeekendAbbo && $feestdag)) {
            $prijsPerKM = 0;
        } else {
            $prijsPerKM *= 0.6;
        }
    }
    else{
        if($studentWeekAbbo && !$feestdag) {
            $prijsPerKM = 0;
        } else{
            $prijsPerKM *= 0.6;
        }
    }

    print ("Je betaalt " . $prijsPerKM . " per km.");
}

function opdracht10() {
    $iemandJarig = False;
    $aantalWekenZonderFeest = 3;
    $zomervakantie = True;

    if ($iemandJarig || $zomervakantie && $aantalWekenZonderFeest > 2) {
        print("deze week is het feest");
    } else {
        print("deze week is er geen feest");
    }
}

function opdracht11() {
    $aantalKoffie = 0;
    $koffieIsVers = False;
    $laatInDeAvond = True;

    if($aantalKoffie == 0) {
        print("Ik neem koffie");
    } else {
        if ($aantalKoffie > 4 || $laatInDeAvond || !$koffieIsVers) {
            print("Ik neem geen koffie");
        } else {
            print("Ik neem koffie");
        }
    }
}

function opdracht12() {
    $hetIsOchtend = True;
    $mamaBelt = True;
    $ikSlaap = False;

    if($ikSlaap) {
        print ("Niet opnemen");
    }
    elseif($hetIsOchtend && $mamaBelt) {
        print("Wel opnemen");

    }else{
        print("Wel opnemen");
    }
}

function opdracht13() {
    // Kijk opdracht 9
}

function opdracht14() {
    $aantal = 9;
    $gebied = "";
    if($aantal >= 3 && $aantal <= 5) {
        print("Mark is humeurig.");
    }else{
        switch($gebied) {
            case ('onderwijs'):
                print("Mark is boos"); break;
            case ('zorg'):
                print("Mark is verdrietig"); break;
            case ('defensie'):
                print("Mark is blij"); break;
            default:
                if ($aantal < 3) {
                    print("Mark is boos");
                } else{
                    print("Mark is blij");
                }
        }
    }
}

